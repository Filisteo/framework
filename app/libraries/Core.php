<?php


class Core {

    protected $controladorActual ="Paginas"; //Controlador por defecto
   
    protected $metodoActual = "index"; // Método por defecto
   
    protected $parametros = []; // Por defecto no hay parámetros
   

    public function __construct() {
   
        $url = $this->getURL();

        //Comprueba que en el directorio controllers, exista un archivo que le pasamos por $url el cual
        //es un array y cuyo indice 0 almacena el nombre del controlador.
        if (file_exists('../app/controllers/' . ucwords($url[0]) . '.php')) {

            //De existir el controlador, lo asigna como controlador actual.
            $this->controladorActual = ucwords($url[0]);

            // Y elimina el dato almacenado en el array.
            unset($url[0]);
        }
        
        //De no existir, utiliza el controlador por defecto (Paginas.php)
        require_once '../app/controllers/' . $this->controladorActual . '.php';

        //E instancia un nuevo controlador
        $this->controladorActual = new $this->controladorActual;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // Si $url, posee un dato en su indice 1, que son los métodos a utilizar en un controlador.
        if (isset($url[1])) {
                //Comprueba que, ese método existe en el controlador seleccionado
                if (method_exists($this->controladorActual, $url[1])) {
                    //Si existe, asigna como método actual, el dato almacenado en el indice 1 del array $url
                    $this->metodoActual = $url[1];
                    // Y elimina el dato almacenado en el array.
                    unset($url[1]);
                }
        }
            //Si no existe, muestra el método actual.
            echo $this->metodoActual;
    
        //El valor del atributo parametros de la calse Core, pasa a ser, si hay parametros en la $url.
        //almacenalos en un array, si no, crea un array vacio.
        $this->parametros = $url ? array_values($url) : [];

        //Llama a la llamada de retorno dada por el primer parámetro callback con los parámetros de param_arr.
        //Se utiliza para no hacer una llamada a cada método y cada parámetro. 
        call_user_func_array([$this->controladorActual, $this->metodoActual], $this->parametros);
    }
   
    public function getURL(){
        
        /*En el htaccsess del directorio raíz suprime, redirecciona directamente a la carpeta public, y 
        almacena todo lo que haya luego desde esa carpeta en la varieble $1  
        
        Luego, accediendo a la carpeta public, a partir de la redirección del htaccess a otro
        htaccess, accedemos index.php pasando un parametro url (index.php?url=$1).

        Luego construimos un nuevo objeto Core, que un cuyo constructor hace una referencia la método getURL.

        El método getURL hace un echo del método GET y de la variable url.

        */
        
        if (isset($_GET['url'])) { //Si existe un método GET de url

                $url = rtrim($_GET['url'], '/'); // Retira los espacios en blanco
           
                $url = filter_var($url, FILTER_SANITIZE_URL); // aplica un filtro a una variable
           
                $url = explode('/', $url); // Devuelve un array de string, siendo cada uno un substring del parámetro 
                //string formado por la división realizada por los delimitadores indicados en el parámetro string separator. 
           
            } else {
           
                $url[0] = $this->controladorActual; // hace referencia al controlador
           
            }
           
            return $url;
        
        
        //echo $_GET["url"];
   
    }
    
   
   }
   
   

?>