<?php

class Controller {


    public function __construct()
    {
        
    }

    public function model ($modelo) {

        require_once '../app/models/'.$modelo.'.php';
        // Carga el modelo
        $modelo = new Post();

        return $modelo;
        //Instancia el modelo y lo devuelve
    }

    public function view ($vista, $datos=[]) {

       if(file_exists('../app/views/' . $vista . '.php')){
            require_once '../app/views/' . $vista . '.php';
            echo $datos['titulo'];
        }else{
            echo "No existe la vista";
        }

    }

}

    

?>