<?php

//Como su nombre indica, el autoloader está destinado a cargar de forma automática las clases utilizadas.
include_once 'helpers/utils.php';    

require_once 'config/config.php';

spl_autoload_register(function ($className) {
    require_once 'libraries/'.$className.'.php';
});

require_once 'controllers/Paginas.php';
?>